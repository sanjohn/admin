<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Models\RequestBooking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RequestBookingController extends BaseController
{
    public function get(Request $request): Response
    {
        $page = $request->input('page' , 1);
        $pageSize = $request->input('pageSize' , 20);
        $order = 'CAST(created_at AS UNSIGNED) DESC';
        $rooms = RequestBooking::orderBy('created_at', 'desc')->where([
            'user_id' => $this->createdBy(),
            'is_delete' => 0,
        ])->paginate($pageSize);
        return $this->success($rooms);
    }
    public function saveRequestBooking(Request $request): Response
    {
        $data = $request->all();
        $fail = RequestBooking::getNotPassValidator($data);
        if($fail){
            return $this->error('Missing required fields');
        }
    //    if(isset($data['id']) && $data['id'] || !RequestBooking::getInfo([['id' , '=' , $data['id'] ] ])){
    //        $info = RequestBooking::getInfo([['booking_date' , '=' , $data['booking_date'] ] ]);
    //        if($info && $info['id'] != $data['id']) {
    //            return $this->error('Room not avaible!');
    //        }
    //    }
        $from = [
            'id' => $data['id'] ?? null,
            'branch_id' => $data['branch_id'],
            'bed' => $data['bed'],
            'price' => $data['price'],
            'request_name' => $data['request_name'],
            'request_phone' => $data['request_phone'],
            'request_date' => $data['request_date'],
            'customer_pay_by' => $data['customer_pay_by'],
            'number_of_customer' => $data['number_of_customer'],
            'request_from' => $data['request_from'],
            'user_id' => $data['user_id'],
//            'sort' => (int)$data['sort'] ?? 1,
        ];
        $id = RequestBooking::saveInfo($from);
        return $this->success($id);
    }
    public function deleteRequestBooking(Request $request): Response
    {
        $id = $request->input('id' , null);
        RequestBooking::deleteInfo($id);
        return $this->success($id);
    }
}
