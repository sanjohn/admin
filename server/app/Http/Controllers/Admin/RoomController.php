<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Room;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class RoomController extends BaseController
{
    public function get(Request $request): Response
    {
//        $where = [['author' , '=', $this->createdByAdmin(), ['hotel_id' => $request->hotel_id]]];
        $page = $request->input('page' , 1);
        $pageSize = $request->input('pageSize' , 20);
        $order = 'CAST(sort AS UNSIGNED) ASC';
        $rooms = Room::where([
           'author' => $this->createdByAdmin(),
           'branch_id' => $request['branch_id'],
           'is_delete' => 0,
        ])->paginate($pageSize);
        return $this->success($rooms);
    }

    public function saveRoom(Request $request): Response
    {
        $data = $request->all();
        $fail = Room::getNotPassValidator($data);
        if($fail){
            return $this->error('Missing required fields');
        }
        if(isset($data['id']) && $data['id'] || !Room::getInfo([['id' , '=' , $data['id'] ] ])){
            $info = Room::getInfo([['room_number' , '=' , $data['room_number'] ] ]);
            if($info && $info['id'] != $data['id']) {
                return $this->error('Room number cannot be duplicated!');
            }
        }
        $from = [
            'id' => $data['id'] ?? null,
            'branch_id' => $data['branch_id'],
            'room_number' => $data['room_number'],
            'bed' => $data['bed'],
            'price' => $data['price'],
            'people' => $data['people'],
            'author' => $this->createdByAdmin(),
            'is_visible' => $data['is_visible'],
            'sort' => (int)$data['sort'] ?? 1,
        ];
        $id = Room::saveInfo($from);
        return $this->success($id);
    }

    public function deleteRoom(Request $request)
    {
        $id = $request->input('id' , null);
        Room::deleteInfo($id);
        return $this->success($id);
    }
}
