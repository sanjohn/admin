<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookingController extends BaseController
{
    public function get(Request $request): Response
    {
        $page = $request->input('page' , 1);
        $pageSize = $request->input('pageSize' , 20);
        $roomBooking = $request->input('room_number' , null);
        $order = 'CAST(created_at AS UNSIGNED) DESC';
        $rooms = Booking::orderBy('created_at', 'desc')->where([
            'room_number' => $roomBooking,
            'is_delete' => 0,
        ])->paginate($pageSize);
        return $this->success($rooms);
    }
    public function saveBooking(Request $request): Response
    {
        $data = $request->all();
        $fail = Booking::getNotPassValidator($data);
        if($fail){
            return $this->error('Missing required fields');
        }
       if(isset($data['id']) && $data['id'] || !Booking::getInfo([['id' , '=' , $data['id'] ] ])){
           $info = Booking::getInfo([['booking_date' , '=' , $data['booking_date'] ] ]);
           if($info && $info['id'] != $data['id']) {
               return $this->error('Room not avaible!');
           }
       }
        $from = [
            'id' => $data['id'] ?? null,
            'branch_id' => $data['branch_id'],
            'room_number' => $data['room_number'],
            'customer_name' => $data['customer_name'],
            'customer_phone' => $data['customer_phone'],
            'booking_date' => $data['booking_date'],
            'customer_pay_status' => $data['customer_pay_status'],
            'customer_pay_by' => $data['customer_pay_by'],
            'number_of_customer' => $data['number_of_customer'],
            'is_visible' => $data['is_visible'],
//            'sort' => (int)$data['sort'] ?? 1,
        ];
        $id = Booking::saveInfo($from);
        return $this->success($id);
    }
    public function deleteBooking(Request $request): Response
    {
        $id = $request->input('id' , null);
        Booking::deleteInfo($id);
        return $this->success($id);
    }
}
