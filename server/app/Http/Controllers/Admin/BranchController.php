<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Branch;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BranchController extends BaseController
{
    public function get(Request $request): Response
    {
        $where = [['author' , '=', $this->createdByAdmin()]];
        $page = $request->input('page' , 1);
        $pageSize = $request->input('pageSize' , 20);
        $order = 'CAST(sort AS UNSIGNED) ASC';
        $branches = Branch::getListData($where, ['*'], $page, $pageSize, $order);
        return $this->success($branches);
    }

    public function saveBranch(Request $request): Response
    {
        $data = $request->all();
        $fail = Branch::getNotPassValidator($data);
        if($fail){
            return $this->error('Missing required fields');
        }
        if(isset($data['id']) && $data['id'] || !Branch::getInfo([['id' , '=' , $data['id'] ] ])){
            $info = Branch::getInfo([['name' , '=' , $data['name'] ] ]);
            if($info && $info['id'] != $data['id']) {
                return $this->error('Name already exist!');
            }
        }
        if ($data['image']) {
            $address = $this->fileUpload($data['image'], 'uploads/');
        }
        $from = [
            'id' => $data['id'] ?? null,
            'name' => $data['name'],
            'author' => $this->createdByAdmin(),
            'branch' => $data['branch'],
            'image' => $address ?? $data['image'],
            'is_visible' => $data['is_visible'],
            'sort' => (int)$data['sort'] ?? 1,
        ];
        $id = Branch::saveInfo($from);
        return $this->success($id);
    }

    public function deleteBranch(Request $request): Response
    {
        $id = $request->input('id' , null);
        Branch::deleteInfo($id);
        return $this->success($id);
    }
}
