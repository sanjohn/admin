<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Room extends BaseModel
{
    use HasFactory;
    protected $table = 'rooms';
    protected $fillable = [
        'branch_id',
        'room_number',
        'bed',
        'people',
        'sort',
        'price',
        'author',
        'is_visible',
        'is_delete',
    ];
    protected $rules = [
        'room_number' => 'required',
        'bed' => 'required',
        'price' => 'required',
        'people' => 'required',
    ];
    protected $casts = [
        'is_visible' => 'boolean',
    ];
    protected static $initBase;
    public static function initBase(): static
    {
        if(!self::$initBase){
            self::$initBase = new static();
        }
        return self::$initBase;
    }
    public function branch(): BelongsTo {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
