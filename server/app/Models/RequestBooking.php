<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RequestBooking extends BaseModel
{
    use HasFactory;
    protected $table = 'request_bookings';
    protected $fillable = [
        'branch_id',
        'user_id',
        'bed',
        'people',
        'request_status',
        'request_phone',
        'request_date',
        'request_name',
        'request_from',
        'number_of_customer',
        'price',
        'is_read',
        'is_delete',
    ];
    protected $rules = [
        'branch_id' => 'required',
        'bed' => 'required',
        'request_phone' => 'required',
        'request_date' => 'required',
        'request_name' => 'required',
        'number_of_customer' => 'required',
        'price' => 'required',
    ];
    protected $casts = [
        'is_read' => 'boolean',
        'request_date' => 'array',
    ];
    protected static $initBase;
    public static function initBase(): static
    {
        if(!self::$initBase){
            self::$initBase = new static();
        }
        return self::$initBase;
    }
    public function booking(): HasMany {
        return $this->hasMany(Booking::class, 'id');
    }
}
