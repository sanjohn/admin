<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Branch extends BaseModel
{
    use HasFactory;
    protected $table = 'branches';
    protected $fillable = [
        'name',
        'author',
        'branch',
        'image',
        'is_visible',
        'is_delete',
        'sort',
    ];
    protected $casts = [
        'is_visible' => 'boolean',
    ];
    protected $rules = [
        'name' => 'required',
        'branch' => 'required',
    ];
    protected static $initBase;
    public static function initBase(){
        if(!self::$initBase){
            self::$initBase = new static();
        }
        return self::$initBase;
    }
}
