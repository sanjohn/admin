<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Booking extends BaseModel
{
    use HasFactory;
    protected $table = 'bookings';
    protected $fillable = [
        'branch_id',
        'room_number',
        'customer_name',
        'customer_phone',
        'booking_date',
        'number_of_customer',
        'customer_pay_status',
        'customer_pay_by',
        'is_visible',
        'is_delete',
    ];
    protected $rules = [
        'branch_id' => 'required',
        'customer_name' => 'required',
        'room_number' => 'required',
        'customer_pay_status' => 'required',
        'customer_pay_by' => 'required',
    ];
    protected $casts = [
        'is_visible' => 'boolean',
        'booking_date' => 'array',
    ];
    protected static $initBase;
    public static function initBase(): static
    {
        if(!self::$initBase){
            self::$initBase = new static();
        }
        return self::$initBase;
    }
    public function room(): BelongsTo {
        return $this->belongsTo(Room::class, 'room_id');
    }
}
