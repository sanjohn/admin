import service from "/@/utils/request";

// 评论管理
export default function  useApi() {
	return {
		getConfiguration: (params?: object) => {
			return service({
				url: 'getConfiguration',
				method: 'get',
				params,
			});
		},
		addConfiguration: (params?: object) => {
			return service.post('saveConfiguration', params);
		},
		updateConfiguration: (params?: object) => {
			return service.post('updateConfiguration', params);
		},
		deleteConfiguration: async (param?: object) => {
			return await service.post('deleteConfiguration', param)
		},
		getAds: (params?: object) => {
			return service({
				url: 'getAdsBanner',
				method: 'get',
				params,
			});
		},
		addAds: (params?: object) => {
			return service.post('saveAds', params);
		},
		updateAds: (params?: object) => {
			return service.post('updateAds', params);
		},
		deleteAds: (params?: object) => {
			return service.post('deleteAds', params);
		},
		getHotel: (params?: object) => {
			return service({
				url: 'getHotel',
				method: 'get',
				params,
			});
		},
		addOrUpdateHotel: (params?: object) => {
			return service.post('saveHotel', params);
		},
		deleteMenuIcon: (params?: object) => {
			return service.post('deleteMenuIcon', params);
		},
		getCategory: (params?: object) => {
			return service({
				url: 'getCategory',
				method: 'get',
				params,
			});
		},
		getRoom: (params?: object) => {
			return service({
				url: 'getRoom',
				method: 'get',
				params,
			});
		},
		addOrUpdateRoom: (params?: object) => {
			return service.post('saveRoom', params);
		},
		deleteRoom: (params?: object) => {
			return service.post('deleteRoom', params);
		},
		addOrUpdateCategory: (params?: object) => {
			return service.post('saveCategory', params);
		},
		updateCategory: (params?: object) => {
			return service.post('updateCategory', params);
		},
		deleteCategory: (params?: object) => {
			return service.post('deleteCategory', params);
		},
		getBookingRoom: (params?: object) => {
			return service({
				url: 'getBookingRoom',
				method: 'get',
				params,
			});
		},
		addUpdateBookingRoom: (params?: object) => {
			return service.post('saveBookingRoom', params);
		},
		deleteBookingRoom: (params?: object) => {
			return service.post('deleteBookingRoom', params);
		},
		getHomeStatistics: (params?: object) => {
			return service({
				url: 'getHomeStatistics',
				method: 'get',
				params,
			});
		},
		getBranch: (params?: object) => {
			return service.get('getBranch', params);
		},
		saveUpdateBranch: (params?: object) => {
			return service.post('saveBranch', params);
		},
		deleteBranch: (params?: object) => {
			return service.post('deleteBranch', params);
		},
	};
}