import IBase from "./IBase";
import functionHelper from "/@/libraries/formatHelpers/functionHelper";

interface IBranch extends IBase {
    name: string,
    branch: string,
    image: string,
    author: string,
}
class Branch implements IBranch {
    id: number;
    sort: number;
    is_visible: number;
    created_at: string;
    updated_at: string
    is_delete: boolean;
    name: string;
    branch: string;
    image: string;
    author: string;
    constructor(init: IBranch) {
        Object.assign(this, init);
        this.created_at = functionHelper.dateStringTo12HourWithTime(init.created_at)
        this.updated_at = functionHelper.dateStringTo12HourWithTime(init.updated_at);
    }
}

export {
    type IBranch,
    Branch,

}