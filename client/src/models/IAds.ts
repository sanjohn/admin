import IBase from "./IBase";

interface IAds extends IBase {
    title: string,
    link: string,
    image: string | undefined,
}
export type { IAds }