interface IBase {
    id: number,
    sort: number,
    is_visible: number,
    created_at: string,
    updated_at: string
    is_delete: boolean,
}
export default IBase;