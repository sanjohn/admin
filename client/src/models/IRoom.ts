import IBase from "./IBase";
import functionHelper from "/@/libraries/formatHelpers/functionHelper";

interface IRoom extends IBase {
    room_number: string,
    price: number,
    bed: number,
    people: string
    author: string,
}
class Room implements IRoom {
    id: number;
    sort: number;
    is_visible: number;
    created_at: string;
    updated_at: string
    is_delete: boolean;
    room_number: string;
    bed: number;
    price: number;
    people: string;
    author: string;
    get priceForDisplay(): string {
        let price = this.price ?? 0.00
        return `${price} $`
    }
    constructor(init: IRoom) {
        Object.assign(this, init);
        this.created_at = functionHelper.dateStringTo12HourWithTime(init.created_at)
        this.updated_at = functionHelper.dateStringTo12HourWithTime(init.updated_at);
    }
}

export {
    type IRoom,
    Room,
}