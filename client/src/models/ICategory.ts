import functionHelper from "../libraries/formatHelpers/functionHelper";
import IBase from "./IBase";

interface ICategory extends IBase{
    name: string,
    key: string,
    image: string,
}
class Category implements ICategory {
    id: number;
    sort: number;
    is_visible: number;
    created_at: string;
    updated_at: string
    is_delete: boolean;
    name: string;
    key: string;
    image: string;
    constructor(init: ICategory) {
        Object.assign(this, init);
        this.created_at = functionHelper.dateStringTo12HourWithTime(init.created_at)
        this.updated_at = functionHelper.dateStringTo12HourWithTime(init.updated_at);
        this.is_visible = init.is_visible
    }
}
export {
    type ICategory,
    Category,
}