import IBase from "./IBase";
import functionHelper from "/@/libraries/formatHelpers/functionHelper";

interface IBooking extends IBase {
    branch_id: number,
    room_number: number,
    customer_name: string,
    customer_phone: string,
    number_of_customer: string
    booking_date: string,
    customer_pay_status: string,
    customer_pay_by: string,
}
class Booking implements IBooking {
    id: number;
    sort: number;
    is_visible: number;
    created_at: string;
    updated_at: string
    is_delete: boolean;
    room_number: number;
    branch_id: number;
    customer_name: string;
    customer_phone: string;
    number_of_customer: string;
    booking_date: string;
    customer_pay_status: string;
    customer_pay_by: string;
    constructor(init: IBooking) {
        Object.assign(this, init);
        this.created_at = functionHelper.dateStringTo12HourWithTime(init.created_at)
        this.updated_at = functionHelper.dateStringTo12HourWithTime(init.updated_at);
    }
}

export {
    type IBooking,
    Booking,
}