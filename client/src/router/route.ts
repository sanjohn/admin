import { RouteRecordRaw } from 'vue-router';

//Extend RouteMeta interface
declare module 'vue-router' {
	interface RouteMeta {
		title?: string;
		isLink?: string;
		isHide?: boolean;
		isKeepAlive?: boolean;
		isAffix?: boolean;
		isIframe?: boolean;
		roles?: string[];
		icon?: string;
	}
}

export const dynamicRoutes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: '/home',
		component: () => import('/@/layout/index.vue'),
		redirect: '/home',
		meta: {
			isKeepAlive: true,
		},
		children: [
			{
				path: '/index',
				name: 'index',
				component: () => import('/@/pages/home/index.vue'),
				meta: {
					title: 'message.router.home',
					isLink: '',
					isHide: false,
					isKeepAlive: true,
					isAffix: true,
					isIframe: false,
					roles: ['admin', 'common'],
					icon: 'iconfont icon-shouye',
				},
			},
			{
				path: '/system',
				name: 'system',
				redirect: '/system/menu',
				meta: {
					title: 'message.router.system',
					isLink: '',
					isHide: false,
					isKeepAlive: true,
					isAffix: false,
					isIframe: false,
					roles: ['admin'],
					icon: 'iconfont icon-xitongshezhi',
				},
				children: [{
					path: '/system/menu',
					name: 'systemMenu',
					component: () => import('/@/pages/system/menu/index.vue'),
					meta: {
						title: 'message.router.systemMenu',
						isLink: '',
						isHide: false,
						isKeepAlive: true,
						isAffix: false,
						isIframe: false,
						roles: ['admin'],
						icon: 'iconfont icon-caidan',
					},
				},
				{
					path: '/system/role',
					name: 'systemRole',
					component: () => import('/@/pages/system/role/index.vue'),
					meta: {
						title: 'message.router.systemRole',
						isLink: '',
						isHide: false,
						isKeepAlive: true,
						isAffix: false,
						isIframe: false,
						roles: ['admin'],
						icon: 'ele-ColdDrink',
					},
				},
				{
					path: '/system/user',
					name: 'systemUser',
					component: () => import('/@/pages/system/user/index.vue'),
					meta: {
						title: 'message.router.systemUser',
						isLink: '',
						isHide: false,
						isKeepAlive: true,
						isAffix: false,
						isIframe: false,
						roles: ['admin'],
						icon: 'iconfont icon-icon-',
					},
				},
				],
			}
		],
	}
];

export const notFoundAndNoPower = [
	{
		path: '/:path(.*)*',
		name: 'notFound',
		component: () => import('/@/views/error/404.vue'),
		meta: {
			title: 'message.staticRoutes.notFound',
			isHide: true,
		},
	},
	{
		path: '/401',
		name: 'noPower',
		component: () => import('/@/views/error/401.vue'),
		meta: {
			title: 'message.staticRoutes.noPower',
			isHide: true,
		},
	},
];

export const staticRoutes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'index',
		component: () => import('/@/pages/home.vue'),
		meta: {
			title: 'home',
			front: true,
		},
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('/@/pages/login/index.vue'),
		meta: {
			title: 'login',
			front: true,
		},
	},
	{
		path: '/visualizingDemo1',
		name: 'visualizingDemo1',
		component: () => import('/@/views/visualizing/demo1.vue'),
		meta: {
			title: 'message.router.visualizingLinkDemo1',
		},
	},
	{
		path: '/visualizingDemo2',
		name: 'visualizingDemo2',
		component: () => import('/@/views/visualizing/demo2.vue'),
		meta: {
			title: 'message.router.visualizingLinkDemo2',
		},
	},
];