import { RouteRecordRaw } from 'vue-router';
import pinia from '/@/stores/index';
import { useUserInfo } from '/@/stores/userInfo';
import { useRequestOldRoutes } from '/@/stores/requestOldRoutes';
import { Session } from '/@/utils/storage';
import { NextLoading } from '/@/utils/loading';
import { dynamicRoutes, notFoundAndNoPower } from '/@/router/route';
import { formatTwoStageRoutes, formatFlatteningRoutes, router } from '/@/router/index';
import { useRoutesList } from '/@/stores/routesList';
import { useTagsViewRoutes } from '/@/stores/tagsViewRoutes';
import { useMenuApi } from '/@/api/menu';
import sysRoute from '/@/router/sys';

//Introduce api request interface
const menuApi = useMenuApi();
const layoutModules : any = import.meta.glob('../layout/routerView/*.{vue,tsx}');
const viewsModules : any = import.meta.glob('../pages/**/*.{vue,tsx}');
const dynamicViewsModules : Record<string, Function> = Object.assign({}, { ...layoutModules }, { ...viewsModules });

export async function initBackEndControlRoutes() {
	// 界面 loading 动画开始执行
	if (window.nextLoading === undefined) NextLoading.start();
	// Interface loading animation starts execution
	if (!Session.get('token')) return false;
	await useUserInfo().setUserInfos();
	// Get routing menu data
	const res = await getBackEndControlRoutes();
	// When there is no login permission, add judgment
	if (res.data.length <= 0) return Promise.resolve(true);
	res.data = generateMenuTree(res.data);
	// 存储接口原始路由（未处理component），根据需求选择使用
	await useRequestOldRoutes().setRequestOldRoutes(JSON.parse(JSON.stringify(res.data)));
	// Process routing (component) and replace the route of the first top-level children of dynamicRoutes (/@/router/route)
	dynamicRoutes[0].children = await backEndComponent(res.data);
	// if(import.meta.env.MODE == 'development') { // @ts-ignore
	// 	dynamicRoutes[0].children.push(sysRoute);
	// }
	// Add dynamic routing
	await setAddRoute();
	await setFilterMenuAndCacheTagsViewRoutes();
}

export async function setFilterMenuAndCacheTagsViewRoutes() {
	const storesRoutesList = useRoutesList(pinia);
	await storesRoutesList.setRoutesList(dynamicRoutes[0].children as any);
	setCacheTagsViewRoutes();
}

export function setCacheTagsViewRoutes() {
	const storesTagsView = useTagsViewRoutes(pinia);
	storesTagsView.setTagsViewRoutes(formatTwoStageRoutes(formatFlatteningRoutes(dynamicRoutes))[0].children);
}

export function setFilterRouteEnd() {
	let filterRouteEnd : any = formatTwoStageRoutes(formatFlatteningRoutes(dynamicRoutes));
	// notFoundAndNoPower prevents 404 and 401 from not being in the layout. If not set, the 404 and 401 interface will be displayed in full screen
	// Related issues No match found for location with path 'xxx'
	filterRouteEnd[0].children = [...filterRouteEnd[0].children, ...notFoundAndNoPower];
	return filterRouteEnd;
}

export async function setAddRoute() {
	await setFilterRouteEnd().forEach((route : RouteRecordRaw) => {
		router.addRoute(route);
	});
}

export async function getBackEndControlRoutes() {
	return await menuApi.getAdminMenu();
}
export function generateMenuTree(menuData: any) {
	const menuMap: any = {}; // Used to build menu maps to find parent menus
	const menuTree: any = []; // The final generated menu tree
	// First, add all menu items to the menu map
	menuData.forEach((menu: any) => {
		menuMap[menu.id] = menu;
		menu.children = []; // Initialize submenu
	});
	// Then, traverse the menu items and insert the submenu into the children of its parent
	menuData.forEach((menu: any) => {
		if (menu.menuSuperior && menuMap[menu.menuSuperior]) {
			menuMap[menu.menuSuperior].children.push(menu);
		} else {
			menuTree.push(menu); // If there is no parent, make it the root menu
		}
	});
	return menuTree;
}
export async function setBackEndControlRefreshRoutes() {
	await getBackEndControlRoutes();
}

export function backEndComponent(routes : any) {
	if (!routes) return;
	let rou = routes.map((item : any) => {
		if (item.component) item.component = dynamicImport(dynamicViewsModules, item.component as string);
		item.children && backEndComponent(item.children);
		return item;
	});
	return  rou;
}

export function dynamicImport(dynamicViewsModules : Record<string, Function>, component : string) {
	const keys = Object.keys(dynamicViewsModules);
	const matchKeys = keys.filter((key) => {
		const k = key.replace(/..\/pages|../, '');
		return k.startsWith(`${component}`) || k.startsWith(`/${component}`);
	});
	if (matchKeys?.length === 1) {
		const matchKey = matchKeys[0];
		return dynamicViewsModules[matchKey];
	}
	if (matchKeys?.length > 1) {
		return false;
	}
}