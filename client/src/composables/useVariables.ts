import { ref } from 'vue';
import { FormInstance } from 'element-plus';
import useApi from "/@/api/api";
import { useI18n } from "vue-i18n";
import { Hide, View } from '@element-plus/icons-vue'
import { useRoute, useRouter } from 'vue-router';

const useVariable = () => {
    const { t } = useI18n();
    const isLoading = ref( false );
    const isProcessing = ref( false );
    const ruleFormRef = ref<FormInstance>();
    const dialog = ref(false);
    const api = useApi();
    const route = useRoute();
    const router = useRouter();
    // const { width, height } = useWindowSize();
    const resetForm = (formEl: FormInstance | undefined) => {
        if (!formEl) return
        formEl.resetFields();
    };
    const openDialogRef = ref();
    const onOpenAddDialog = (type: string) => {
        openDialogRef.value.openDialog(type);
    };
    const onOpenEditDialog = (type: string, row: object) => {
        openDialogRef.value.openDialog(type, row);
    };
    const fileList = ref([] as any);
    const renderFunc = ref();
    return {
        t,
        isLoading,
        isProcessing,
        ruleFormRef,
        dialog,
        resetForm,
        openDialogRef,
        fileList,
        onOpenAddDialog,
        onOpenEditDialog,
        api,
        renderFunc,
        Hide, View,
        route, router,
    };
};

export default useVariable;
