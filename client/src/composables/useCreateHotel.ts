import { onMounted, reactive } from "vue";
import EnumApiErrorCode from "/@/models/enums/enumApiErrorCode";
import EnumMessageType from "/@/models/enums/enumMessageType";
import { messageNotification } from "/@/libraries/elementUiHelpers/notificationHelper";
import { Category, ICategory } from "/@/models/ICategory";
import useVariable from "/@/composables/useVariables";

export default function useCreateHotel() {
	const state = reactive({
		data: [],
		page: {
			page: 1,
			pageSize: 10,
		},
		total: 0,
	});
	const { t, api, openDialogRef, onOpenAddDialog, onOpenEditDialog, isLoading } = useVariable();
	const getTableData = async () => {
		isLoading.value =  true;
		const response = await api.getHotels();
		if (response.code !== EnumApiErrorCode.success) {
			// eslint-disable-next-line no-console
			console.log(response);
		} else {
			state.data = response.data.data.map((item: ICategory) => new Category(item));
			state.total = response.data.total;
		}
			isLoading.value = false;
	};
	const deleteRow = async (row: Object) => {
		const response = await api.deleteCategory(row)
		if (response.code === EnumApiErrorCode.success) {
			messageNotification(t('message.success'), EnumMessageType.Success);
			getTableData();
		} else {
			messageNotification(response.message, EnumMessageType.Error);
		}
	};
	const onSystem = (row: object, key: string) => {
		if (key === 'edit') {
			onOpenEditDialog(key, row);
		} else {
			deleteRow(row)
		}
	};
	const onHandlePageChange = (data: any) => {
		state.page.pageSize = data.pageSize;
		state.page.page = data.page;
		getTableData();
	};
	onMounted(() => {
		getTableData();
	})
	return {
		onOpenAddDialog,
		onOpenEditDialog,
		openDialogRef,
		state,
		getTableData,
		deleteRow,
		onHandlePageChange,
		onSystem,
	}
}